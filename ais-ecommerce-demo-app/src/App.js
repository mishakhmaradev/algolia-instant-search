import React from 'react';
import algoliasearch from 'algoliasearch/lite';
import {
  InstantSearch,
  Hits,
  SearchBox,
  Pagination,
  Highlight,
} from 'react-instantsearch-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const searchClient = algoliasearch(
  'B1G2GM9NG0',
  'aadef574be1f9252bb48d4ea09b5cfe5'
);

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 50px;
`;

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 800px;
  height: 500px;
  background: rgb(0, 8, 213);
  background: linear-gradient(
    90deg,
    rgba(0, 8, 213, 1) 0%,
    rgba(146, 7, 132, 1) 46%,
    rgba(255, 0, 69, 1) 95%
  );
  margin-bottom: 20px;
`;
const Title = styled.h1`
  color: white;
  margin-bottom: 30px;
`;

const StyledInput = styled(SearchBox)`
  display: flex;
  justify-content: center;
  svg {
    display: none;
  }
  input {
    width: 500px;
    height: 50px;
    border-radius: 15px;
    border: none;
  }
  button {
    width: 90px;
    height: 38px;
    position: absolute;
    top: 25px;
    left: 400px;
    color: #ffffff;
    background-color: blue;

    padding: 10px;
    border-radius: 12px;
    border: none;
  }
  .ais-SearchBox-reset {
    display: none;
  }

  button:before {
    content: 'Search';
  }
`;

const PaginationWrapper = styled.div`
  margin-top: 20px;
`;

function App() {
  return (
    <AppWrapper>
      <InstantSearch searchClient={searchClient} indexName="demo_ecommerce">
        <InputWrapper>
          <Title>Test Application </Title>
          <StyledInput
            searchAsYouType={false}
            translations={{
              placeholder: 'Search for anything...',
            }}
          />
        </InputWrapper>
        <Hits hitComponent={Hit} />

        <PaginationWrapper>
          <Pagination />
        </PaginationWrapper>
      </InstantSearch>
    </AppWrapper>
  );
}

function Hit(props) {
  return (
    <article>
      <Highlight attribute="name" hit={props.hit} />
    </article>
  );
}

Hit.propTypes = {
  hit: PropTypes.object.isRequired,
};

export default App;
